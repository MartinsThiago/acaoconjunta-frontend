'use strict';

/**
 * @ngdoc overview
 * @name acaoconjuntaFrontendApp
 * @description
 * # acaoconjuntaFrontendApp
 *
 * Main module of the application.
 */
angular
  .module('acaoconjuntaFrontendApp', [
    'ngAnimate',
    'ngSanitize'
  ]);
