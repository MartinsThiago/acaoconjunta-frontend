'use strict';

/**
 * @ngdoc function
 * @name acaoconjuntaFrontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the acaoconjuntaFrontendApp
 */
angular.module('acaoconjuntaFrontendApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
